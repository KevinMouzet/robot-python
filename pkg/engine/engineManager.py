import threading
from adafruit_motorkit import MotorKit
import time

class EngineManager():

    LEFT_POWER = 0
    RIGHT_POWER = 0

    REFRESH_INTERVAL_S = 0.05

    motorkit = None

    start = False
    
    def __init__(self):
        self.motorkit = MotorKit()
        self.run()

    def onStop(self):
        self.POWER = 0
        self.applyChanges()
        self.start = False

    def onStart(self):
        self.start = True

    def run(self):
        def func_wrapper():
            self.run()
            if self.start == True:
                self.applyChanges()
        t = threading.Timer(self.REFRESH_INTERVAL_S, func_wrapper)
        t.start()

    def updateLeftPower(self, hundredBasedPower):
        self.LEFT_POWER = self.getCleanedPower(hundredBasedPower)

    def updateRightPower(self, hundredBasedPower):
        self.RIGHT_POWER = self.getCleanedPower(hundredBasedPower)

    def getCleanedPower(self, hundredBasedPower):
        if hundredBasedPower > 0:
            if hundredBasedPower > 10:
                return (hundredBasedPower / 200) + 0.5
        else:
            if hundredBasedPower < -10:
                return (hundredBasedPower / 200) - 0.5
        return 0

    def applyChanges(self):
        self.motorkit.motor1.throttle = self.LEFT_POWER
        self.motorkit.motor2.throttle = self.LEFT_POWER
        
        self.motorkit.motor3.throttle = self.RIGHT_POWER
        self.motorkit.motor4.throttle = self.RIGHT_POWER
        