from enum import Enum, unique

@unique
class XboxOnePadBtnCode(Enum):
    BTN_A = 304
    BTN_B = 305
    BTN_X = 307
    BTN_Y = 308
    BTN_LB = 310
    BTN_RB = 311
    TRIGGER_LT = 2
    TRIGGER_RT = 5
    BTN_SELECT = 314
    BTN_START = 315
    BTN_MODE = 316
    JOYSTICK_L_CLIC = 317
    JOYSTICK_R_CLIC = 318
    ARROW_X = 17
    ARROW_Y = 16
    JOYSTICK_L_X = 0
    JOYSTICK_L_Y = 1
    JOYSTICK_R_Y = 4
    JOYSTICK_R_X = 3
    
