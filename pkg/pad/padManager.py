from evdev import InputDevice, categorize, ecodes, ff
import time
from pad.models.xboxOnePadBtnCode import XboxOnePadBtnCode
from engine.engineManager import EngineManager


class PadManager():

    JOYSTICK_MAX = 32768
    TRIGGER_MAX = 1023

    PAD_PATH_MSI = '/dev/input/event19'
    PAD_PATH_PI = '/dev/input/event0'

    def __init__(self):
        self.xboxOnePad = InputDevice(self.PAD_PATH_PI)
        self.controlActived = False
        self.engineManager = EngineManager()

    def handlePadEvents(self):
        for event in self.xboxOnePad.read_loop():
            self.mapPadEvent(event)

    def mapPadEvent(self, event):            
        if event.code == XboxOnePadBtnCode.BTN_START.value:
            if self.translateButtonEvent(event) == 0:
                self.toggleControlState()
                return
        elif self.controlActived == False:
            return
        elif event.code == XboxOnePadBtnCode.JOYSTICK_L_Y.value:
            translatedEvent = self.translateJoystickEvent(event)
            self.emitLeftPower(translatedEvent)
            return
        elif event.code == XboxOnePadBtnCode.JOYSTICK_R_Y.value:
            translatedEvent = self.translateJoystickEvent(event)
            self.emitRightPower(translatedEvent)
            return
        elif event.code == XboxOnePadBtnCode.TRIGGER_LT.value:
            self.translateTriggerEvent(event)
            return
        elif event.code == XboxOnePadBtnCode.BTN_B.value:
            self.translateButtonEvent(event)
            return
        elif event.code == XboxOnePadBtnCode.BTN_X.value:
            self.translateButtonEvent(event)
            return
        elif event.code == XboxOnePadBtnCode.BTN_Y.value:
            self.translateButtonEvent(event)
            return

    def translateButtonEvent(self, event):
        return event.value

    def translateJoystickEvent(self, event):
        if event.type == 3:
            hundredBasedValue = round(event.value * 100 / self.JOYSTICK_MAX)
            return hundredBasedValue * -1

    def translateTriggerEvent(self, event):
        hundredBasedValue = round(event.value * 100 / self.TRIGGER_MAX)
        return hundredBasedValue

    def toggleControlState(self):
        self.rumblePad()
        self.controlActived = not self.controlActived
        if (self.controlActived == True):
            self.startEngines()
        else:
            self.stopEngines()

    # emit events to engine manager

    def startEngines(self):
        self.engineManager.onStart()

    def stopEngines(self):
        self.engineManager.onStop()

    def emitLeftPower(self, hundredbasedPower):
        if hundredbasedPower == None:
            return
        self.engineManager.updateLeftPower(hundredbasedPower)

    def emitRightPower(self, hundredbasedPower):
        if hundredbasedPower == None:
            return
        self.engineManager.updateRightPower(hundredbasedPower)

    # rumble effet

    def rumblePad(self):
        rumble = ff.Rumble(strong_magnitude=0x0000, weak_magnitude=0xffff)
        effect_type = ff.EffectType(ff_rumble_effect=rumble)
        duration_ms = 300

        effect = ff.Effect(
            ecodes.FF_RUMBLE, -1, 0,
            ff.Trigger(0, 0),
            ff.Replay(duration_ms, 0),
            effect_type
        )

        repeat_count = 1
        effect_id = self.xboxOnePad.upload_effect(effect)
        self.xboxOnePad.write(ecodes.EV_FF, effect_id, repeat_count)
        time.sleep(1)
        self.xboxOnePad.erase_effect(effect_id)
